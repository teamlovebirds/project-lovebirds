package com.project.lovebirds.projectlovebirds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button bUnlock;

    Intent toLoginScreen;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bUnlock = (Button) findViewById(R.id.unlock);

        bUnlock.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                toLoginScreen = new Intent("Lovebirds.Login");
                startActivity(toLoginScreen);
            }
        });
    }
}
