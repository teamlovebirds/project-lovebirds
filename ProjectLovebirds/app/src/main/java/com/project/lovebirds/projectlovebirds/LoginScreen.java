package com.project.lovebirds.projectlovebirds;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

public class LoginScreen extends AppCompatActivity {

    private Button blogin;

    Intent toHomePage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        blogin = (Button) findViewById(R.id.bLogin);

        blogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toHomePage = new Intent("Lovebirds.HomePage");
                startActivity(toHomePage);
            }
        });
    }
}
